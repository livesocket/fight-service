package migrations

import (
	"github.com/jmoiron/sqlx"
)

func CreateFighterTable(tx *sqlx.Tx) error {
	_, err := tx.Exec("CREATE TABLE `fighter` (`username` varchar(255),`wins` int UNSIGNED DEFAULT 0 NOT NULL,`losses` int UNSIGNED DEFAULT 0 NOT NULL, PRIMARY KEY(`username`))")
	return err
}
