package migrations

import (
	"github.com/jmoiron/sqlx"
)

func CreateFightsTable(tx *sqlx.Tx) error {
	_, err := tx.Exec("CREATE TABLE `fights` (`attacker` varchar(255) NOT NULL,`defender` varchar(255) NOT NULL, PRIMARY KEY(`attacker`,`defender`))")
	return err
}
