package migrations

import (
	"github.com/jmoiron/sqlx"
)

func CreateFightConfigTable(tx *sqlx.Tx) error {
	_, err := tx.Exec("CREATE TABLE `fight_config` (`channel` varchar(255),`enabled` tinyint UNSIGNED DEFAULT 1 NOT NULL,`cooldown` int UNSIGNED DEFAULT 60 NOT NULL, `updated_at` datetime DEFAULT CURRENT_TIMESTAMP NOT NULL, `updated_by` varchar(255) NOT NULL, PRIMARY KEY(`channel`))")
	return err
}
