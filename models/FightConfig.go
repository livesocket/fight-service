package models

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	"gitlab.com/livesocket/service"
)

type FightConfig struct {
	Channel   string     `db:"channel" json:"channel"`
	Enabled   bool       `db:"enabled" json:"enabled"`
	Cooldown  uint       `db:"cooldown" json:"cooldown"`
	UpdatedAt *time.Time `db:"updated_at" json:"updated_at"`
	UpdatedBy string     `db:"updated_by" json:"updated_by"`
}

func NewFightConfig(channel string, updatedBy string) *FightConfig {
	now := time.Now()
	return &FightConfig{
		Channel:   channel,
		Enabled:   true,
		Cooldown:  60,
		UpdatedAt: &now,
		UpdatedBy: updatedBy,
	}
}

func FindFightConfig(channel string) (*FightConfig, error) {
	config := FightConfig{}
	err := service.DB.Get(&config, "SELECT * FROM `fight_config` WHERE `channel`=?", channel)
	if err != nil {
		if strings.Contains(fmt.Sprint(err), "no rows") {
			return nil, nil
		}
		return nil, err
	}
	return &config, nil
}

func (config *FightConfig) Create() (sql.Result, error) {
	return service.DB.NamedExec("INSERT INTO `fight_config` (`channel`,`enabled`,`cooldown`,`updated_at`,`updated_by`) VALUES (:channel,:enabled,:cooldown,:updated_at,:updated_by)", config)
}

func (config *FightConfig) Update() (sql.Result, error) {
	return service.DB.NamedExec("UPDATE `fight_config` SET `enabled`=:enabled,`cooldown`=:cooldown,`updated_at`=:updated_at,`updated_by`=:updated_by WHERE `channel`=:channel", config)
}

func (config *FightConfig) Destroy() (sql.Result, error) {
	return service.DB.NamedExec("DELETE FROM `fight_config` WHERE `channel`=:channel", config)
}
