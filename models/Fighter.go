package models

import (
	"database/sql"
	"fmt"
	"strings"

	"gitlab.com/livesocket/service"
)

type Fighter struct {
	Username string `db:"username" json:"username"`
	Wins     uint   `db:"wins" json:"wins"`
	Losses   uint   `db:"losses" json:"losses"`
}

func NewFighter(username string) *Fighter {
	return &Fighter{
		Username: username,
		Wins:     0,
		Losses:   0,
	}
}

func FindFighter(username string) *Fighter {
	// query for fighter
	fighter := Fighter{}
	err := service.DB.Get(&fighter, "SELECT * FROM `fighter` WHERE `username`=?", username)
	if err != nil {
		if strings.Contains(fmt.Sprint(err), "no rows") {
			fighter = *NewFighter(username)
			fighter.Create()
		}
	}
	return &fighter
}

func (fighter *Fighter) Create() (sql.Result, error) {
	return service.DB.NamedExec("INSERT INTO `fighter` (`username`,`wins`,`losses`) VALUES (:username,:wins,:losses)", fighter)
}

func (fighter *Fighter) Update() (sql.Result, error) {
	return service.DB.NamedExec("UPDATE `fighter` SET `wins`=:wins,`losses`=:losses WHERE `username`=:username", fighter)
}

func (fighter *Fighter) AddWin() (sql.Result, error) {
	fighter.Wins += 1
	return fighter.Update()
}

func (fighter *Fighter) AddLoss() (sql.Result, error) {
	fighter.Losses += 1
	return fighter.Update()
}
