package models

import (
	"database/sql"
	"fmt"
	"strings"

	"gitlab.com/livesocket/service"
)

type Fight struct {
	Attacker string `db:"attacker" json:"attacker"`
	Defender string `db:"defender" json:"defender"`
}

func NewFight(attacker string, defender string) *Fight {
	return &Fight{
		Attacker: attacker,
		Defender: defender,
	}
}

func FindAcceptableFight(defender string) *Fight {
	// query for fighter
	fight := Fight{}
	err := service.DB.Get(&fight, "SELECT * FROM `fights` WHERE `defender`=?", defender)
	if err != nil {
		if strings.Contains(fmt.Sprint(err), "no rows") {
			return nil
		}
	}
	return &fight
}

func (fight *Fight) Create() (sql.Result, error) {
	return service.DB.NamedExec("INSERT INTO `fights` (`attacker`,`defender`) VALUES (:attacker,:defender)", fight)
}

func (fight *Fight) Destroy() (sql.Result, error) {
	return service.DB.NamedExec("DELETE FROM `fights` WHERE `attacker`=:attacker AND `defender`=:defender", fight)
}
