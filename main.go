package main

import (
	"log"
	"os"
	"os/signal"

	"gitlab.com/livesocket/fight-service/actions"
	"gitlab.com/livesocket/fight-service/commands"
	"gitlab.com/livesocket/fight-service/migrations"
	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/service/lib"
)

func main() {
	close, err := service.NewStandardService([]lib.Action{
		actions.GetFighterAction,
		actions.CreateFightAction,
		actions.AcceptFightAction,
		actions.RematchAction,
		actions.ResetFightsAction,
		actions.GetConfigAction,
		actions.UpdateConfigAction,
		commands.FightCommand,
		commands.FightStatsCommand,
		commands.RematchCommand,
	}, nil, "__fight_service", migrations.CreateFighterTable, migrations.CreateFightConfigTable, migrations.CreateFightsTable)
	defer close()
	if err != nil {
		panic(err)
	}

	// Wait for CTRL-c or client close while handling events.
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)
	select {
	case <-sigChan:
	case <-service.Socket.Done():
		log.Print("Router gone, exiting")
		return
	}
}
