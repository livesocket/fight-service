package commands_test

import (
	"testing"

	"github.com/gammazero/nexus/v3/wamp"
	"github.com/gempir/go-twitch-irc/v2"
	"gitlab.com/livesocket/fight-service/commands"
	"gitlab.com/livesocket/fight-service/migrations"
	"gitlab.com/livesocket/fight-service/models"
	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/service/lib"
)

func TestRematch(t *testing.T) {
	close := service.NewTestService([]lib.Action{
		commands.RematchCommand,
		lib.Action{Proc: "private.fight.rematch", Handler: lib.MockArgsResult(models.Fighter{Username: "testuser"}, models.Fighter{Username: "otheruser"})},
	}, nil, "__test", migrations.CreateFighterTable)
	defer close()

	// Set initial state
	message := &twitch.PrivateMessage{
		User: twitch.User{
			Name: "testuser",
		},
	}

	// Call endpoint
	result, err := service.Socket.SimpleCall("command.rematch", wamp.List{"testuser"}, wamp.Dict{"message": message})
	if err != nil {
		t.Error(err)
	}

	if result.Arguments[0].(string) != "testuser wins!" {
		t.Error("Should be the response message")
	}
}
