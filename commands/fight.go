package commands

import (
	"errors"
	"fmt"

	"github.com/gammazero/nexus/v3/client"
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/conv"
	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/service/lib"
)

// FightCommand Creates or accepts a fight
//
// To accept a pending fight
// !fight
// To challenge someone to a fight
// !fight <username>
//
// command.fight
// [defender string]{message twitch.PrivateMessage}
//
// Returns [string] as response for chat
var FightCommand = lib.Action{
	Proc:    "command.fight",
	Handler: fight,
}

type fightInput struct {
	Attacker string
	Defender string
}

func fight(invocation *wamp.Invocation) client.InvokeResult {
	// Get input args from call
	input, err := getFightInput(invocation)
	if err != nil {
		return lib.ErrorResult(err)
	}

	// If a defender is specified then create a new fight
	if input.Defender != "" {
		// Create the fight
		_, err = service.Socket.SimpleCall("private.fight.create", nil, wamp.Dict{"attacker": input.Attacker, "defender": input.Defender})
		if err != nil {
			return lib.ErrorResult(err)
		}
		// Return challenge message
		return lib.ArgsResult(fmt.Sprintf("%s you have been challenged to a fight. Use \"!fight\" to accept", input.Defender))
	}
	// No defender specified, attempt to accept a pending fight
	result, err := service.Socket.SimpleCall("private.fight.accept", nil, wamp.Dict{"accepter": input.Attacker})
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Parse winner and loser names
	winner := result.Arguments[0].(map[string]interface{})["username"].(string)
	loser := result.Arguments[1].(map[string]interface{})["username"].(string)

	// Return message to display in chat
	return lib.ArgsResult(fmt.Sprintf("%s wins! %s want a \"!rematch\"?", winner, loser))
}

func getFightInput(invocation *wamp.Invocation) (*fightInput, error) {
	var attacker string
	var defender string
	if invocation.ArgumentsKw["message"] == nil {
		return nil, errors.New("Missing message")
	}

	attacker = conv.ToString(invocation.ArgumentsKw["message"].(map[string]interface{})["User"].(map[string]interface{})["Name"])
	if len(invocation.Arguments) > 0 {
		defender = conv.ToString(invocation.Arguments[0])
	}

	return &fightInput{
		Attacker: attacker,
		Defender: defender,
	}, nil
}
