package commands

import (
	"errors"
	"fmt"

	"github.com/gammazero/nexus/v3/client"
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/conv"
	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/service/lib"
)

// RematchCommand Rematches a fight
//
// !rematch
//
// command.rematch
// {message twitch.PrivateMessage}
//
// Returns [string] as response for chat
var RematchCommand = lib.Action{
	Proc:    "command.rematch",
	Handler: rematch,
}

type rematchInput struct {
	Username string
}

func rematch(invocation *wamp.Invocation) client.InvokeResult {
	// Get input args from call
	input, err := getRematchInput(invocation)
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Get winner of rematch
	result, err := service.Socket.SimpleCall("private.fight.rematch", nil, wamp.Dict{"username": input.Username})
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Return message to display in chat
	return lib.ArgsResult(fmt.Sprintf("%s wins!", result.Arguments[0].(map[string]interface{})["username"]))
}

func getRematchInput(invocation *wamp.Invocation) (*rematchInput, error) {
	if invocation.ArgumentsKw["message"] == nil {
		return nil, errors.New("Missing message")
	}

	return &rematchInput{
		Username: conv.ToString(invocation.ArgumentsKw["message"].(map[string]interface{})["User"].(map[string]interface{})["Name"]),
	}, nil
}
