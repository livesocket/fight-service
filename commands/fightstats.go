package commands

import (
	"errors"
	"fmt"

	"github.com/gammazero/nexus/v3/client"
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/conv"
	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/service/lib"
)

// FightStatsCommand Gets fight stats for user
//
// !fightstats
//
// command.fightstats
// {message twitch.PrivateMessage}
//
// Returns [string] as response for chat
var FightStatsCommand = lib.Action{
	Proc:    "command.fightstats",
	Handler: fightStats,
}

type fightStatsInput struct {
	Username string
}

func fightStats(invocation *wamp.Invocation) client.InvokeResult {
	// Get input args from call
	input, err := getFightStatsInput(invocation)
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Find fighter
	result, err := service.Socket.SimpleCall("private.fighter.get", nil, wamp.Dict{"username": input.Username})
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Check if fight history exists
	if len(result.Arguments) == 0 {
		return lib.ArgsResult("No fight history found")
	}

	// Get fighter stats
	fighter := result.Arguments[0].(map[string]interface{})
	wins := fighter["wins"].(uint64)
	losses := fighter["losses"].(uint64)

	// Return message to display in chat
	return lib.ArgsResult(fmt.Sprintf("%s your wins/losses are %d/%d", input.Username, wins, losses))
}

func getFightStatsInput(invocation *wamp.Invocation) (*fightStatsInput, error) {
	if invocation.ArgumentsKw["message"] == nil {
		return nil, errors.New("Missing message")
	}

	return &fightStatsInput{
		Username: conv.ToString(invocation.ArgumentsKw["message"].(map[string]interface{})["User"].(map[string]interface{})["Name"]),
	}, nil
}
