package commands_test

import (
	"testing"

	"github.com/gammazero/nexus/v3/wamp"
	"github.com/gempir/go-twitch-irc/v2"
	"gitlab.com/livesocket/fight-service/commands"
	"gitlab.com/livesocket/fight-service/migrations"
	"gitlab.com/livesocket/fight-service/models"
	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/service/lib"
)

func TestFightFunctionalChallenge(t *testing.T) {
	close := service.NewTestService([]lib.Action{
		commands.FightCommand,
		lib.Action{Proc: "private.fight.create", Handler: lib.MockArgsResult()},
	}, nil, "__test", migrations.CreateFighterTable)
	defer close()

	// Set initial state
	message := &twitch.PrivateMessage{
		User: twitch.User{
			Name: "attacker",
		},
	}

	// Call endpoint
	result, err := service.Socket.SimpleCall("command.fight", wamp.List{"defender"}, wamp.Dict{"message": message})
	if err != nil {
		t.Error(err)
	}

	if result.Arguments[0].(string) != "defender you have been challenged to a fight. Use !fight to accept" {
		t.Error("Should be the response message")
	}
}

func TestFightAccept(t *testing.T) {
	close := service.NewTestService([]lib.Action{
		commands.FightCommand,
		lib.Action{Proc: "private.fight.accept", Handler: lib.MockArgsResult(models.Fighter{Username: "attacker"}, models.Fighter{Username: "defender"})},
	}, nil, "__test", migrations.CreateFighterTable)
	defer close()

	// Set initial state

	message := &twitch.PrivateMessage{
		User: twitch.User{
			Name: "attacker",
		},
	}

	// Call endpoint
	result, err := service.Socket.SimpleCall("command.fight", nil, wamp.Dict{"message": message})
	if err != nil {
		t.Error(err)
	}

	if result.Arguments[0].(string) != "attacker wins! defender want a \"!rematch\"?" {
		t.Error("Should be the response message")
	}
}
