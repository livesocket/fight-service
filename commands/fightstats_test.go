package commands_test

import (
	"testing"

	"github.com/gammazero/nexus/v3/wamp"
	"github.com/gempir/go-twitch-irc/v2"
	"gitlab.com/livesocket/fight-service/commands"
	"gitlab.com/livesocket/fight-service/migrations"
	"gitlab.com/livesocket/fight-service/models"
	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/service/lib"
)

func TestFightStats(t *testing.T) {
	var fighter *models.Fighter
	close := service.NewTestService([]lib.Action{
		commands.FightStatsCommand,
		lib.Action{Proc: "private.fighter.get", Handler: lib.MockArgsResult(&fighter)},
	}, nil, "__test", migrations.CreateFighterTable)
	defer close()

	// Set initial state

	fighter = models.NewFighter("testuser")
	fighter.Wins = 77
	fighter.Losses = 75
	_, err := fighter.Create()
	if err != nil {
		t.Error(err)
	}

	message := &twitch.PrivateMessage{
		User: twitch.User{
			Name: "testuser",
		},
	}

	// Call endpoint
	result, err := service.Socket.SimpleCall("command.fightstats", wamp.List{"defender"}, wamp.Dict{"message": message})
	if err != nil {
		t.Error(err)
	}

	if result.Arguments[0].(string) != "testuser your wins/losses are 77/75" {
		t.Error("Should be the response message")
	}
}
