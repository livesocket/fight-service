module gitlab.com/livesocket/fight-service

go 1.12

require (
	github.com/gammazero/nexus v2.1.2+incompatible
	github.com/gammazero/nexus/v3 v3.0.0
	github.com/gempir/go-twitch-irc v1.1.0 // indirect
	github.com/gempir/go-twitch-irc/v2 v2.2.1
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/google/go-cmp v0.3.1 // indirect
	github.com/google/wire v0.3.0 // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-isatty v0.0.10 // indirect
	gitlab.com/livesocket/conv v0.0.0-20191012101209-727e1c03a95c
	gitlab.com/livesocket/service v1.4.3
	golang.org/x/net v0.0.0-20191109021931-daa7c04131f5 // indirect
	golang.org/x/sys v0.0.0-20191110163157-d32e6e3b99c4 // indirect
	golang.org/x/tools v0.0.0-20191109212701-97ad0ed33101 // indirect
	golang.org/x/xerrors v0.0.0-20191011141410-1b5146add898 // indirect
)
