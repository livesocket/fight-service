FROM golang:alpine as base
RUN apk --no-cache add git ca-certificates g++
WORKDIR /repos/fight-service
ADD go.mod go.sum ./
RUN go mod download

FROM base as builder
WORKDIR /repos/fight-service
ADD . .
RUN CGO_ENABLED=0 GOOS=linux go build -o fight-service

FROM scratch as release
COPY --from=builder /repos/fight-service/fight-service /fight-service
EXPOSE 8080
ENTRYPOINT ["/fight-service"]