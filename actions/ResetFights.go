package actions

import (
	"github.com/gammazero/nexus/v3/client"
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/service/lib"
)

// ResetFightsAction Resets all pending fights and rematches
//
// private.fight.reset
//
// Returns nothing
var ResetFightsAction = lib.Action{
	Proc:    "private.fight.reset",
	Handler: resetFights,
}

func resetFights(invocation *wamp.Invocation) client.InvokeResult {
	// Reset fights
	Pendings = map[string]string{}
	Rematches = map[string]string{}

	// Return nothing
	return lib.ArgsResult()
}
