package actions_test

import (
	"testing"

	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/fight-service/actions"
	"gitlab.com/livesocket/fight-service/migrations"
	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/service/lib"
)

func TestRematchFunctional(t *testing.T) {
	close := service.NewTestService([]lib.Action{
		actions.RematchAction,
	}, nil, "__test", migrations.CreateFighterTable)
	defer close()

	rematcher := "rematchuser"
	defender := "defenduser"

	// Set initial state
	actions.Rematches[rematcher] = defender

	// Call endpoint
	_, err := service.Socket.SimpleCall("private.fight.rematch", nil, wamp.Dict{"username": rematcher})
	if err != nil {
		t.Error(err)
	}

	if actions.Rematches[rematcher] != "" {
		t.Error("Should have destroyed rematch")
	}

	// cleanup
	actions.Rematches = map[string]string{}
	actions.Pendings = map[string]string{}
}
