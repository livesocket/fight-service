package actions

import (
	"errors"

	"github.com/gammazero/nexus/v3/client"
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/fight-service/models"
	"gitlab.com/livesocket/service/lib"
)

// GetFighterAction Gets a fighter
//
// private.fighter.get
// {username string}
//
// Returns nothing
var GetFighterAction = lib.Action{
	Proc:    "private.fighter.get",
	Handler: getFighter,
}

type getFighterInput struct {
	Username string
}

func getFighter(invocation *wamp.Invocation) client.InvokeResult {
	// Get input args from call
	input, err := getGetFighterInput(invocation.ArgumentsKw)
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Find fighter
	fighter := models.FindFighter(input.Username)
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Return fighter
	return lib.ArgsResult(fighter)
}

func getGetFighterInput(kwargs wamp.Dict) (*getFighterInput, error) {
	if kwargs["username"] == nil {
		return nil, errors.New("Missing username")
	}
	return &getFighterInput{
		Username: kwargs["username"].(string),
	}, nil
}
