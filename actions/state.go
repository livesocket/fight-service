package actions

import (
	"math/rand"
	"time"
)

var Pendings map[string]string = map[string]string{}
var Rematches map[string]string = map[string]string{}

func RollWinner(a string, b string) (string, string) {
	rand.Seed(time.Now().UnixNano())
	if a == "snarechops" {
		return b, "snarechops"
	} else if b == "snarechops" {
		return a, "snarechops"
	} else {
		// flip the coin
		side := rand.Intn(2)
		if side == 0 {
			return a, b
		}
		return b, a
	}
}
