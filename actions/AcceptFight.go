package actions

import (
	"errors"
	"log"

	"github.com/gammazero/nexus/v3/client"
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/fight-service/models"
	"gitlab.com/livesocket/service/lib"
)

// AcceptFightAction Accepts a fight
//
// private.fight.accept
// {accepter string}
//
// Returns [winner Fighter, loser Fighter]
var AcceptFightAction = lib.Action{
	Proc:    "private.fight.accept",
	Handler: acceptFight,
}

type acceptFightInput struct {
	Accepter string
}

func acceptFight(invocation *wamp.Invocation) client.InvokeResult {
	// Get input args from call
	input, err := getAcceptFightInput(invocation.ArgumentsKw)
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Find attacker from stored fight
	attacker := Pendings[input.Accepter]

	// Check if pending fight exists
	if attacker == "" {
		return lib.ErrorResult("No pending fight to accept")
	}

	delete(Pendings, input.Accepter)

	// Roll winner
	winner, loser := RollWinner(attacker, input.Accepter)
	log.Printf("winner: %v, loser: %v", winner, loser)

	// Store fight for rematch
	Rematches[loser] = winner

	// Save winner record
	wFighter := models.FindFighter(winner)
	_, err = wFighter.AddWin()
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Save loser record
	lFighter := models.FindFighter(loser)
	_, err = lFighter.AddLoss()
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Return result of fight
	return lib.ArgsResult(wFighter, lFighter)
}

func getAcceptFightInput(kwargs wamp.Dict) (*acceptFightInput, error) {
	if kwargs["accepter"] == nil {
		return nil, errors.New("Missing accepter")
	}

	return &acceptFightInput{
		Accepter: kwargs["accepter"].(string),
	}, nil
}
