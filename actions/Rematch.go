package actions

import (
	"errors"

	"github.com/gammazero/nexus/v3/client"
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/fight-service/models"
	"gitlab.com/livesocket/service/lib"
)

// RematchAction Rolls a rematch
//
// private.fight.rematch
// {username string}
//
// Returns [winner Fighter, loser Fighter]
var RematchAction = lib.Action{
	Proc:    "private.fight.rematch",
	Handler: rematch,
}

type rematchInput struct {
	Username string
}

func rematch(invocation *wamp.Invocation) client.InvokeResult {
	// Get input args from call
	input, err := getRematchInput(invocation.ArgumentsKw)
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Find match in stored rematches
	defender := Rematches[input.Username]

	if defender == "" {
		return lib.ErrorResult("No rematch available")
	}

	// Delete the stored rematch
	delete(Rematches, input.Username)

	// Roll winner
	winner, loser := RollWinner(input.Username, defender)

	// Save winner record
	wFighter := models.FindFighter(winner)
	_, err = wFighter.AddWin()
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Save loser record
	lFighter := models.FindFighter(loser)
	_, err = lFighter.AddLoss()
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Return fight result
	return lib.ArgsResult(wFighter, lFighter)
}

func getRematchInput(kwargs wamp.Dict) (*rematchInput, error) {
	if kwargs["username"] == nil {
		return nil, errors.New("Missing username")
	}
	return &rematchInput{
		Username: kwargs["username"].(string),
	}, nil
}
