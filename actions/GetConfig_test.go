package actions_test

import (
	"testing"

	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/fight-service/actions"
	"gitlab.com/livesocket/fight-service/migrations"
	"gitlab.com/livesocket/fight-service/models"
	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/service/lib"
)

func TestGetConfigFunctional(t *testing.T) {
	close := service.NewTestService([]lib.Action{
		actions.GetConfigAction,
	}, nil, "__test", migrations.CreateFightConfigTable)
	defer close()

	config := models.NewFightConfig("testchannel", "moduser")
	_, err := config.Create()
	if err != nil {
		t.Error(err)
	}

	result, err := service.Socket.SimpleCall("public.fight.config.get", nil, wamp.Dict{"channel": "testchannel"})
	if err != nil {
		t.Error(err)
	}

	if len(result.Arguments) == 0 {
		t.Error("Should have found the fight config")
	}

	if result.Arguments[0].(map[string]interface{})["channel"].(string) != "testchannel" {
		t.Error("Should have found the fight config for the correct channel")
	}

	// cleanup
}
