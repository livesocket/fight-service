package actions_test

import (
	"testing"

	"gitlab.com/livesocket/fight-service/actions"
	"gitlab.com/livesocket/fight-service/migrations"
	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/service/lib"
)

func TestResetFightsFunctional(t *testing.T) {
	close := service.NewTestService([]lib.Action{
		actions.ResetFightsAction,
	}, nil, "__test", migrations.CreateFighterTable)
	defer close()

	// Set initial state
	actions.Pendings["a"] = "a"
	actions.Rematches["b"] = "b"

	// Call endpoint
	_, err := service.Socket.SimpleCall("private.fight.reset", nil, nil)
	if err != nil {
		t.Error(err)
	}

	if actions.Pendings["a"] != "" {
		t.Error("Should have reset the pendings")
	}

	if actions.Rematches["b"] != "" {
		t.Error("Should have reset the rematches")
	}
}
