package actions

import (
	"errors"

	"github.com/gammazero/nexus/v3/client"
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/service/lib"
)

// CreateFightAction Creates a fight for acceptance
//
// private.fight.create
// {attacker string, defender string}
//
// Returns nothing
var CreateFightAction = lib.Action{
	Proc:    "private.fight.create",
	Handler: createFight,
}

type createFightInput struct {
	Attacker string
	Defender string
}

func createFight(invocation *wamp.Invocation) client.InvokeResult {
	// Get input args from call
	input, err := getCreateFightInput(invocation.ArgumentsKw)
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Create fight entry in memory
	Pendings[input.Defender] = input.Attacker

	// Return success but nothing
	return client.InvokeResult{}
}

func getCreateFightInput(kwargs wamp.Dict) (*createFightInput, error) {
	if kwargs["attacker"] == nil {
		return nil, errors.New("Missing attacker")
	}

	if kwargs["defender"] == nil {
		return nil, errors.New("Missing defender")
	}

	return &createFightInput{
		Attacker: kwargs["attacker"].(string),
		Defender: kwargs["defender"].(string),
	}, nil
}
