package actions_test

import (
	"testing"

	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/fight-service/actions"
	"gitlab.com/livesocket/fight-service/migrations"
	"gitlab.com/livesocket/fight-service/models"
	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/service/lib"
)

func TestGetFighterFunctional(t *testing.T) {
	close := service.NewTestService([]lib.Action{
		actions.GetFighterAction,
	}, nil, "__test", migrations.CreateFighterTable)
	defer close()

	fighter := models.NewFighter("fightuser")
	_, err := fighter.Create()
	if err != nil {
		t.Error(err)
	}

	result, err := service.Socket.SimpleCall("private.fighter.get", nil, wamp.Dict{"username": "fightuser"})
	if err != nil {
		t.Error(err)
	}

	if len(result.Arguments) == 0 {
		t.Error("Should have found a fighter")
	}

	if result.Arguments[0].(map[string]interface{})["username"].(string) != "fightuser" {
		t.Error("Should have found the right fighter")
	}
}
