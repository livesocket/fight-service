package actions_test

import (
	"testing"

	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/fight-service/actions"
	"gitlab.com/livesocket/fight-service/migrations"
	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/service/lib"
)

func TestAcceptFightFunctional(t *testing.T) {
	close := service.NewTestService([]lib.Action{
		actions.AcceptFightAction,
	}, nil, "__test", migrations.CreateFighterTable)
	defer close()

	actions.Pendings["testuser"] = "otheruser"

	result, err := service.Socket.SimpleCall("private.fight.accept", nil, wamp.Dict{"accepter": "testuser"})
	if err != nil {
		t.Error(err)
	}

	args := result.Arguments
	winner := args[0].(map[string]interface{})
	loser := args[1].(map[string]interface{})

	if (winner["username"] != "testuser" && loser["username"] != "otheruser") && (winner["username"] != "otheruser" && loser["username"] != "testuser") {
		t.Error("Invalid result")
	}

	// cleanup
	actions.Pendings = map[string]string{}
}
