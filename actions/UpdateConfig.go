package actions

import (
	"errors"

	"github.com/gammazero/nexus/v3/client"
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/fight-service/models"
	"gitlab.com/livesocket/service/lib"
)

// UpdateConfigAction Updates fight config
//
// private.fight.config.update
// {channel string, enabled bool, cooldown uint64, username string}
//
// Returns [Config]
var UpdateConfigAction = lib.Action{
	Proc:    "public.fight.config.update",
	Handler: updateConfig,
}

type updateConfigInput struct {
	Channel  string
	Enabled  bool
	Cooldown uint
	Username string
}

func updateConfig(invocation *wamp.Invocation) client.InvokeResult {
	input, err := getUpdateConfigInput(invocation.ArgumentsKw)
	if err != nil {
		return lib.ErrorResult(err)
	}

	config, err := models.FindFightConfig(input.Channel)
	if err != nil {
		return lib.ErrorResult(err)
	}

	config.Enabled = input.Enabled
	config.Cooldown = input.Cooldown
	config.UpdatedBy = input.Username
	config.Update()

	config, err = models.FindFightConfig(input.Channel)
	if err != nil {
		return lib.ErrorResult(err)
	}

	return lib.ArgsResult(config)
}

func getUpdateConfigInput(kwargs wamp.Dict) (*updateConfigInput, error) {
	if kwargs["channel"] == nil {
		return nil, errors.New("Missing channel")
	}
	if kwargs["enabled"] == nil {
		return nil, errors.New("Missing enabled")
	}
	if kwargs["cooldown"] == nil {
		return nil, errors.New("Missing cooldown")
	}
	if kwargs["username"] == nil {
		return nil, errors.New("Missing username")
	}
	return &updateConfigInput{
		Channel:  kwargs["channel"].(string),
		Enabled:  kwargs["enabled"].(bool),
		Cooldown: kwargs["cooldown"].(uint),
		Username: kwargs["username"].(string),
	}, nil
}
