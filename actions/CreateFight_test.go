package actions_test

import (
	"testing"

	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/fight-service/actions"
	"gitlab.com/livesocket/fight-service/migrations"
	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/service/lib"
)

func TestCreateFightFunctional(t *testing.T) {
	close := service.NewTestService([]lib.Action{
		actions.CreateFightAction,
	}, nil, "__test", migrations.CreateFighterTable)
	defer close()

	actions.Pendings["testuser"] = "otheruser"

	_, err := service.Socket.SimpleCall("private.fight.create", nil, wamp.Dict{"attacker": "auser", "defender": "duser"})
	if err != nil {
		t.Error(err)
	}

	if actions.Pendings["duser"] != "auser" {
		t.Error("Should have created the fight")
	}

	// cleanup
	actions.Pendings = map[string]string{}
}
