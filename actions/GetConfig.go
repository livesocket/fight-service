package actions

import (
	"errors"

	"github.com/gammazero/nexus/v3/client"
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/fight-service/models"
	"gitlab.com/livesocket/service/lib"
)

// GetConfigAction Gets config settings for fights
//
// private.fight.config.get
// {channel string}
//
// Returns nothing
var GetConfigAction = lib.Action{
	Proc:    "public.fight.config.get",
	Handler: getConfig,
}

type getConfigInput struct {
	Channel string
}

func getConfig(invocation *wamp.Invocation) client.InvokeResult {
	// Get input args from call
	input, err := getGetConfigInput(invocation.ArgumentsKw)
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Find fight config for channel
	config, err := models.FindFightConfig(input.Channel)
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Return fight config
	return lib.ArgsResult(config)
}

func getGetConfigInput(kwargs wamp.Dict) (*getConfigInput, error) {
	if kwargs["channel"] == nil {
		return nil, errors.New("Missing channel")
	}
	return &getConfigInput{
		Channel: kwargs["channel"].(string),
	}, nil
}
